var uuid = require('uuid');
var util = require("util");
var crypto = require('crypto');
var format = require('biguint-format');

var Client = require('node-rest-client').Client;
 
var client = new Client();
 
//var endpoint = "http://requestb.in/16y8zz21";
var endpoint = "https://stg.api.payulatam.com/payments-api/4.0/service.cgi";

var merchantId = "500238";
var apiKey = "6u39nqhq8ftd0hlvnjfs66eh8c";
var apiLogin = "11959c415b33d0c";
var accountId = "500546"; // Test account id for Peru

var tx_value = 10;
var currency = "PEN";

var referenceCode = format(crypto.randomBytes(8), 'dec');

// apiKey~merchantId~referenceCode~tx_value~currency​
var signature_text = apiKey + "~" + merchantId + "~" + referenceCode + "~" + tx_value + "~" + currency;

var signature = crypto.createHash('md5').update(signature_text).digest('hex');

var user_id = uuid.v4();

var headers = {
	"Content-Type": "application/json"
};

var data = {
   "language": "es",
   "command": "SUBMIT_TRANSACTION",
   "merchant": {
      "apiKey": apiKey,
      "apiLogin": apiLogin
   },
   "transaction": {
      "order": {
         "accountId": accountId,
         "referenceCode": referenceCode,
         "description": "Payment Test - " + referenceCode,
         "language": "es",
         "signature": signature,
         "notifyUrl": "http://requestb.in/of4x2pof",
         "additionalValues": {
            "TX_VALUE": {
               "value": tx_value,
               "currency": currency
            }
         },
         "buyer": {
            "merchantBuyerId": user_id, // Store User Identifier
            "fullName": "Juan Alberto Perez Rios",
            "emailAddress": "juan.perez.01@mailinator.com",
            "dniNumber": "10203040"
         }
      },
      "payer": {
         "merchantPayerId": user_id,
         "fullName": "Juan Alberto Perez Rios",
         "emailAddress": "juan.perez.01@mailinator.com",
         "contactPhone": "511998877665",
      },
      "creditCard": {
         "number": "4012888888881881",
         "securityCode": "123",
         "expirationDate": "2020/01",
         "name": "JUAN ALBERTO PEREZ RIOS"
      },
      "extraParameters": {
         "INSTALLMENTS_NUMBER": 1
      },
      "type": "AUTHORIZATION_AND_CAPTURE",
      "paymentMethod": "VISA",
      "paymentCountry": "PE"
    },
   "test": true
};

// set content-type header and data as json in args parameter 
var args = {
  data: data,
  headers: headers,
  responseConfig:{
  	timeout:1000 //response timeout 
  }
};
 
var req = client.post(endpoint, args, function(data,response) {
	console.log("RESPONSE BODY:\n");
    // parsed response body as js object 
    //console.log(data.toString('utf8'));
    console.log(util.inspect(data, {showHidden: false, depth: null}));

    console.log("\nRESPONSE OBJECT:\n");
    // raw response 
    console.log(response);
});

req.on('responseTimeout',function(res){
    console.log('response has expired');
    
});
 
//it's usefull to handle request errors to avoid, for example, socket hang up errors on request timeouts 
req.on('error', function(err){
    console.log('request error',err);
});