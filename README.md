PayU Payments API Test
======================

Base on information from this links:

* [1] http://www.payu.com.pe/en/developers
* [2] http://developers.payulatam.com/en/api/payments.html
* [3] http://developers.payulatam.com/en/sdk/sandbox.html
* [4] http://developers.payulatam.com/es/foros/
* [5] https://www.paypalobjects.com/en_US/vhelp/paypalmanager_help/credit_card_numbers.htm

Since the PayU server has a self-signed certificate you need to run the script llike this:

```
npm install
NODE_TLS_REJECT_UNAUTHORIZED=0 node payu_test.js
```

(c) 2015 - Antonio Ognio <antonio@ognio.com>
